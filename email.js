require("dotenv").config();
var nodemailer = require("nodemailer");

var transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "wdpf1252386@gmail.com",
    // App Password
    pass: `${process.env.App_password}`,
  },
});

var mailOptions = {
  from: "wdpf1252386@gmail.com",
  to: "dishco.bd@gmail.com",
  subject: "Sending Email using Node.js",
  text: "That was easy! Happy Learning",
};

transporter.sendMail(mailOptions, function (error, info) {
  if (error) {
    console.log(error);
  } else {
    console.log("Email sent: " + info.response);
  }
});
