var http = require("http");
var fs = require("fs");

http
  .createServer(function (req, res) {
    /******  Read File */
    //   fs.readFile('./demo.html', function(err, data) {
    //     res.writeHead(200, {'Content-Type': 'text/html'});
    //     res.write(data);
    //     return res.end();
    //   });
    /****** Create/Update File */
    /** The fs.appendFile() method appends specified content to a file. If the file does not exist, the file will be created: */
    // fs.appendFile("mynewfile1.txt", "Hello content!", function (err) {
    //   if (err) throw err;
    //   console.log("Saved!");
    // });
    /** The fs.open() method takes a "flag" as the second argument, if the flag is "w" for "writing", the specified file is opened for writing. If the file does not exist, an empty file is created */
    // fs.open("demofile.txt", "w", function (err, file) {
    //   if (err) throw err;
    //   console.log("Saved!");
    //   res.end();
    // });
    /** The fs.writeFile() method replaces the specified file and content if it exists. If the file does not exist, a new file, containing the specified content, will be created */
    // fs.writeFile("demofile.txt", "Hello content!", function (err) {
    //   if (err) throw err;
    //   console.log("Saved!");
    // });
    /******** Delete Files*/
    // fs.unlink("demofile.txt", function (err) {
    //   if (err) throw err;
    //   console.log("File deleted!");
    // });
    /******** Rename Files */
    // fs.rename("demo.php", "demo.html", function (err) {
    //   if (err) throw err;
    //   console.log("File Renamed!");
    // });
  })
  .listen(8080);
