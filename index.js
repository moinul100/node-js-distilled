var http = require("http");
var url = require("url");

http
  .createServer(function (req, res) {
    // Here, `req` represents the incoming HTTP request.
    // `res` represents the HTTP response you will send back to the client.
    res.writeHead(200, { "Content-Type": "text/html" });
    //Return the url part of the request object:
    // res.write(req.url);
    // res.end("<br/> Hello World");

    // Fetch from url via parameter
    var q = url.parse(req.url, true).query;
    var txt = q.year + " " + q.month;
    res.end(txt);
  })
  .listen(8080);
